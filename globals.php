<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="Global Settings" executable="0" order="28"> 
    <cms:editable name="site_name" label="Site's name" type="text" />
    <cms:editable name="site_desc" label="Site's description" type="textarea" />
    <cms:editable name="site_tagline" label="Site's Tag Line" type="text" />
    <cms:editable name="site_keywords" label="Site's keywords" type="textarea" />
    <cms:editable name="site_url" label="Site's URL" type="text" />
    <cms:editable name="og_image" type="image" label='OG Image (1200px X 630px)' />

    <cms:editable name="site_fb" label="Facebook page link" type="text" />
    <cms:editable name="site_twitter" label="Twitter page link" type="text" />
    <cms:editable name="site_instagram" label="Instagram page link" type="text" />
    <cms:editable name="site_youtube" label="Youtube page link" type="text" />

    <cms:editable name="main_tel" type="text" label='Office Telephone no.' />
    <cms:editable name="main_mobile" type="text" label='Office Mobile no.' />
    <cms:editable name="main_address" type="textarea" label='Office Address' />
    <cms:editable name="main_email" type="text" label='Office Email address' />

    <cms:editable name="newsletter_footer_caption" type="text" label='Newsletter footer caption' />

    <cms:editable name="news_banner_image" type="image" label='News Banner Image (1680px X 580px)' />

</cms:template>
<?php COUCH::invoke(); ?>