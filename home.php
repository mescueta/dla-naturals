<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="Index/Home">
    <cms:repeatable name='product_list' label="List of Products to dispaly on Homepage" order="1" >
        <cms:editable name="product_list_title" label="Prouct Title" type="text" col_width='100'/>
        <cms:editable type='image' name='product_list_image' label='Product Image' show_preview='1' quality='100' width="200" />
        <cms:editable name="product_list_details" label="Details" type="textarea" />
        <cms:editable name="product_list_link" label="Link" type="text" col_width='150'/>
    </cms:repeatable>
</cms:template>
<cms:if "<cms:get_custom_field 'site_tagline' masterpage='globals.php' />" >
    <cms:set page_title="<cms:get_custom_field 'site_name' masterpage='globals.php' /> | <cms:get_custom_field 'site_tagline' masterpage='globals.php' />" />
<cms:else /> 
    <cms:set page_title="<cms:get_custom_field 'site_name' masterpage='globals.php' />" />
</cms:if>    
<cms:set page_desc="<cms:get_custom_field 'site_desc' masterpage='globals.php' />" />
<cms:set page_image="<cms:get_custom_field 'og_image' masterpage='globals.php' />" />
<cms:set page_name="<cms:get_custom_field 'site_tagline' masterpage='globals.php' />" />

    <cms:embed 'header.html' />
	
    <div class="main-container" id="main">

        <!-- Banner Home -->
        <cms:embed 'slider-home.html' />

        <!-- Products -->
        <section class="default-section products-section section-80 padded-lg">
            <h1 class="section-title">Products</h1>
            <div class="rw cl-4">
                <cms:show_repeatable 'product_list' >
                <div class="cl">
                    <div class="tile tile-plain tile-clickable">
                        <div class="tile-body">
                            <img src="<cms:show product_list_image />" alt="<cms:show k_page_title />" class="thumbnail">
                        </div>
                        <div class="tile-footer">
                            <h2><cms:show product_list_title /></h2>
                            <cms:excerptHTML count="14" ignore="img, table, br, iframe"><p><cms:show product_list_details /></p></cms:excerptHTML>
                        </div>
                        <a href="<cms:show product_list_link />" title="<cms:show product_list_title />" class="wrapped-link"></a>
                    </div>
                </div>
                </cms:show_repeatable>
            </div>   
        </section>

        <!-- Breaker -->
        <cms:embed 'featured-breaker.html' />

        <!-- What's New -->
        <section class="default-section products-section whats-new-section section-80 padded-lg">
            <h1 class="section-title">What's new</h1>
            <div class="rw cl-4">
                <div class="cl">
                    <div class="tile tile-plain tile-clickable">
                        <div class="tile-body">
                            <!-- <div class="floated-content centered">
                                <h2>Recipes</h2>
                            </div> -->
                            <img src="theme/images/WhatsNew-VIDEO-2.jpg" alt="Tile here" class="thumbnail">
                        </div>
                        <div class="tile-footer">
                            <h2>Recipes</h2>
                            <p>Lorem Ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</p>
                        </div>
                        <a href="recipes.html" title="whole link" class="wrapped-link"></a>
                    </div>
                </div>
                <div class="cl">
                    <div class="tile tile-plain tile-clickable">
                        <div class="tile-body">
                            <!-- <div class="floated-content centered">
                                <h2>Events</h2>
                            </div> -->
                            <img src="theme/images/WhatsNew-EVENTS.jpeg" alt="Tile here" class="thumbnail">
                        </div>
                        <div class="tile-footer">
                            <h2>Events</h2>
                            <p>Lorem Ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</p>
                        </div>
                        <a href="#" title="whole link" class="wrapped-link"></a>
                    </div>
                </div>
                <div class="cl">
                    <div class="tile tile-plain tile-clickable">
                        <div class="tile-body">
                            <!-- <div class="floated-content centered">
                                <h2>Videos</h2>
                            </div> -->
                            <img src="theme/images/WhatsNew-RECIPES.jpg" alt="Tile here" class="thumbnail">
                        </div>
                        <div class="tile-footer">
                            <h2>Videos</h2>
                            <p>Lorem Ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</p>
                        </div>
                        <a href="#" title="whole link" class="wrapped-link"></a>
                    </div>
                </div>
                <div class="cl">
                    <div class="tile tile-plain tile-clickable">
                        <div class="tile-body">
                            <!-- <div class="floated-content centered">
                                <h2>News</h2>
                            </div> -->
                            <img src="theme/images/WhatsNew-NEWS.jpg" alt="Tile here" class="thumbnail">
                        </div>
                        <div class="tile-footer">
                            <h2>News</h2>
                            <p>Lorem Ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</p>
                        </div>
                        <a href="#" title="whole link" class="wrapped-link"></a>
                    </div>
                </div>
            </div>   
        </section>
            
    <cms:embed 'footer.html' />
    <!-- /Footer -->    

<?php COUCH::invoke(); ?>    