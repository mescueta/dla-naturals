<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="Index/Home" hidden="0"></cms:template>
<cms:if "<cms:get_custom_field 'site_tagline' masterpage='globals.php' />" >
    <cms:set page_title="<cms:get_custom_field 'site_name' masterpage='globals.php' /> | <cms:get_custom_field 'site_tagline' masterpage='globals.php' />" />
<cms:else /> 
    <cms:set page_title="<cms:get_custom_field 'site_name' masterpage='globals.php' />" />
</cms:if>    
<cms:set page_desc="<cms:get_custom_field 'site_desc' masterpage='globals.php' />" />
<cms:set page_image="<cms:get_custom_field 'og_image' masterpage='globals.php' />" />
<cms:set page_name="<cms:get_custom_field 'site_tagline' masterpage='globals.php' />" />
    <cms:embed 'header.html' />
    
    <div class="main-container" id="main">

        <div class="loader-container" style="display: block;">
            <div class="text-container">
                <img src="theme/images/logo.png" alt="logo" class="loader-logo">
                <p>Create Lasting Memories</p>
            </div>
        </div>

        <!-- Banner Home -->
        <cms:embed 'slider-home.html' />

        <!-- Products -->
        <section class="default-section products-section section-80 section-max padded-lg" id="products_section">
            <h1 class="section-title">Our products</h1>
            <div class="rw cl-4">
                <cms:pages masterpage='products-chocolates.php'>
                <div class="cl">
                    <div class="tile tile-plain tile-clickable">
                        <div class="tile-body">
                            <img src="<cms:show category_thumbnail_image />" alt="Tile here" class="thumbnail">
                        </div>
                        <div class="tile-footer">
                            <h2><cms:show category_name /></h2>
                            <cms:excerptHTML count="14" ignore="img, table, br, h1, h2, iframe"><p><cms:show category_excerpt /></p></cms:excerptHTML>
                        </div>
                        <a href="<cms:show k_page_link />" title="<cms:show k_page_title />" class="wrapped-link"></a>
                    </div>
                </div>
                </cms:pages>
                <cms:pages masterpage='products-fruits.php'>
                <div class="cl">
                    <div class="tile tile-plain tile-clickable">
                        <div class="tile-body">
                            <img src="<cms:show category_thumbnail_image />" alt="Tile here" class="thumbnail">
                        </div>
                        <div class="tile-footer">
                            <h2><cms:show category_name /></h2>
                            <cms:excerptHTML count="14" ignore="img, table, br, h1, h2, iframe"><p><cms:show category_excerpt /></p></cms:excerptHTML>
                        </div>
                        <a href="<cms:show k_page_link />" title="<cms:show k_page_title />" class="wrapped-link"></a>
                    </div>
                </div>
                </cms:pages>
                <cms:pages masterpage='products-sugars.php'>
                <div class="cl">
                    <div class="tile tile-plain tile-clickable">
                        <div class="tile-body">
                            <img src="<cms:show category_thumbnail_image />" alt="Tile here" class="thumbnail">
                        </div>
                        <div class="tile-footer">
                            <h2><cms:show category_name /></h2>
                            <cms:excerptHTML count="14" ignore="img, table, br, h1, h2, iframe"><p><cms:show category_excerpt /></p></cms:excerptHTML>
                        </div>
                        <a href="<cms:show k_page_link />" title="<cms:show k_page_title />" class="wrapped-link"></a>
                    </div>
                </div>
                </cms:pages>
                <cms:pages masterpage='products-nuts.php'>
                <div class="cl">
                    <div class="tile tile-plain tile-clickable">
                        <div class="tile-body">
                            <img src="<cms:show category_thumbnail_image />" alt="Tile here" class="thumbnail">
                        </div>
                        <div class="tile-footer">
                            <h2><cms:show category_name /></h2>
                            <cms:excerptHTML count="14" ignore="img, table, br, h1, h2, iframe"><p><cms:show category_excerpt /></p></cms:excerptHTML>
                        </div>
                        <a href="<cms:show k_page_link />" title="<cms:show k_page_title />" class="wrapped-link"></a>
                    </div>
                </div>
                </cms:pages>
            </div>   
        </section>

        <!-- Breaker -->
        <cms:embed 'featured-breaker.html' />
            
    <cms:embed 'footer.html' />
    <!-- /Footer -->    

<?php COUCH::invoke(); ?>    