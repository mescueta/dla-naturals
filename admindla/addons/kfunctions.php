<?php
if ( !defined('K_COUCH_DIR') ) die(); // cannot be loaded directly

require_once( K_COUCH_DIR.'addons/watermark/watermark.php' );
//require_once( K_COUCH_DIR.'addons/cart/cart.php' );
//require_once( K_COUCH_DIR.'addons/inline/inline.php' );
require_once( K_COUCH_DIR.'addons/extended/extended-folders.php' );
//require_once( K_COUCH_DIR.'addons/extended/extended-comments.php' );
//require_once( K_COUCH_DIR.'addons/extended/extended-users.php' );
//require_once( K_COUCH_DIR.'addons/routes/routes.php' );
//require_once( K_COUCH_DIR.'addons/jcropthumb/jcropthumb.php' );

$FUNCS->register_tag( 'trim', array('CustomTags', 'trim_all') );
class CustomTags{
   
    function trim_all( $str, $node ){

        $res = $str[0]['rhs'];
        return trim( preg_replace( "/\s+/" , ' ' , $res ) , " \t\n\r\0\x0B" );
    }
   
}

/////////// begin <cms:search_ex> ///////////
class SearchEx{
    static function tag_handler( $params, $node ){
        global $CTX, $FUNCS, $TAGS, $DB, $AUTH;

        extract( $FUNCS->get_named_vars(
                   array(
                         'masterpage'=>'',
                         'keywords'=>'',
                        ),
                   $params) );

        $masterpage = trim( $masterpage );
        if( $keywords ){
            $keywords = trim( $keywords );
        }
        else{
            $keywords = trim( $_GET['s'] );
        }
        // is something being searched?
        if( !$keywords ) return;

        // get the keywords being searched
        $keywords = strip_tags( $keywords );
        $orig_keywords = $keywords;
        $keywords = str_replace( array(",", "+", "-", "(", ")"), ' ', $keywords );

        // http://www.askingbox.com/tip/mysql-combine-full-text-search-with-like-search-for-words-with-3-letters
        $searchwords = array_filter( array_map("trim", explode(' ', $keywords)) );
        $longwords = array();
        $shortwords = array();

        foreach( $searchwords as $word ){
            if( mb_strlen($word, 'UTF-8')>3 ){
                $longwords[] = $word;
            }
            else{
                if( mb_strlen($word, 'UTF-8')==3 ){
                    if(!in_array($val,array('and','the','him','her')))
                    $shortwords[] = $word;
                }
            }
        }

        if( !count($shortwords) ){ // no 3 character words - delegate to original cms:search tag
            if( !count($longwords) ) return;

            $keywords = implode (' ', $longwords );

            // delegate to 'pages' tag
            for( $x=0; $x<count($params); $x++ ){
                $param = &$params[$x];
                if( strtolower($param['lhs'])=='keywords' ){
                    $param['rhs'] = $keywords;
                    $added = 1;
                    break;
                }
            }
            if( !$added ){
                $params[] = array('lhs'=>'keywords', 'op'=>'=', 'rhs'=>$keywords);
            }

            $html = $TAGS->pages( $params, $node, 1 );
        }
        else{
            // we have 3 character words to contend with.

            // craft sql query
            // select ..
            $sql = 'SELECT cp.template_id, cp.id, cf.title, cf.content';

            if( count($longwords) ){
                // add the '+' for boolean search
                $sep = "";
                foreach( $longwords as $kw ){
                    $bool_keywords .= $sep . "+" . $kw;
                    $sep = " ";
                }

                $sql .= ", ((MATCH(cf.content) AGAINST ('".$DB->sanitize($bool_keywords)."') * 1) + (MATCH(cf.title) AGAINST ('".$DB->sanitize($bool_keywords)."') * 1.25)) as score";
            }

            // from ..
            $sql .= "\r\n" . 'FROM ';
            $sql .= K_TBL_FULLTEXT ." cf
            inner join  ".K_TBL_PAGES." cp on cp.id=cf.page_id
            inner join ".K_TBL_TEMPLATES." ct on ct.id=cp.template_id";

            // where ..
            $sql .= "\r\n" . 'WHERE ';
            if( count($longwords) ){
                $sql .= " ((MATCH(cf.content) AGAINST ('".$DB->sanitize($bool_keywords)."' IN BOOLEAN MODE) * 1) + (MATCH(cf.title) AGAINST ('".$DB->sanitize($bool_keywords)."' IN BOOLEAN MODE) * 1.25))";
            }
            else{
                $sql .= '1=1';
            }
            if( $masterpage ){
                // masterpage="NOT blog.php, testimonial.php"
                $sql .= $FUNCS->gen_sql( $masterpage, 'ct.name');
            }
            foreach( $shortwords as $kw ){
                $sql .= " AND (cf.content LIKE '%".$DB->sanitize($kw)."%' OR cf.title LIKE '%".$DB->sanitize($kw)."%')";
            }
            if( $hide_future_entries ){
                $sql .= " AND cp.publish_date < '".$FUNCS->get_current_desktop_time()."'";
            }
            $sql .= " AND NOT cp.publish_date = '0000-00-00 00:00:00'";
            $sql .= " AND cp.access_level<='".$AUTH->user->access_level."'";
            $sql .= " AND ct.executable=1";

            // order
            if( count($longwords) ){
                $sql .= "\r\n";
                $sql .= "ORDER BY score DESC";
            }

            // delegate sql to cms:query
            for( $x=0; $x<count($params); $x++ ){
                $param = &$params[$x];
                if( strtolower($param['lhs'])=='sql' ){
                    $param['rhs'] = $sql;
                    $added = 1;
                    break;
                }
            }
            if( !$added ){
                $params[] = array('lhs'=>'sql', 'op'=>'=', 'rhs'=>$sql);
            }
            $params[] = array('lhs'=>'fetch_pages', 'op'=>'=', 'rhs'=>'1');

            $FUNCS->add_event_listener( 'alter_page_tag_context', array('SearchEx', 'ctx_handler') );
            $node->__my_keywords = array_merge( $longwords, $shortwords );
            $html = $TAGS->pages( $params, $node, 5 );
            $FUNCS->remove_event_listener( 'alter_page_tag_context', array('SearchEx', 'ctx_handler') );
        }

        $CTX->set( 'k_search_query', $orig_keywords, 'global' );
        return $html;

    }// end func tag_handler

    static function ctx_handler( $rec, $mode, $params, $node ){
        global $CTX, $FUNCS;

        if( !is_array($node->__my_keywords) ) return;

        $keywords = $node->__my_keywords;
        if( $CTX->get('k_template_is_clonable', true) ){
            $hilited = $FUNCS->hilite_search_terms( $keywords, $rec['title'], 1 );
        }
        else{
            $tpl_title = $CTX->get('k_template_title', true);
            $hilited = $tpl_title ? $tpl_title : $CTX->get('k_template_name', true);
        }
        $CTX->set( 'k_search_title', $hilited );
        $CTX->set( 'k_search_content', $rec['content'] ); //entire content searched

        $hilited = $FUNCS->hilite_search_terms( $keywords, $rec['content'] );
        $CTX->set( 'k_search_excerpt', $hilited ); //hilighted excerpt of searched content
    }
}

$FUNCS->register_tag( 'search_ex', array('SearchEx', 'tag_handler'), 1, 1 );

/////////// end <cms:search_ex> ///////////