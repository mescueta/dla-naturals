<cms:php>

    $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $parts = parse_url($current_url);
    $categorySlug = parse_url($current_url, PHP_URL_PATH);
    parse_str($parts['query'], $query);

    //var_dump($categorySlug);

    global $category;
    global $category_icon;

    if (strpos($categorySlug, "travel") !== false){
        $category = 'travel';
        $category_icon = 'la-map-marker';
    } elseif (strpos($categorySlug, "taste") !== false) {
        $category = 'taste';
        $category_icon = 'la-cutlery';
    } elseif (strpos($categorySlug, "music") !== false) {
        $category = 'music';
        $category_icon = 'la-music';
    } elseif (strpos($categorySlug, "tech") !== false) {
        $category = 'tech';
        $category_icon = 'la-rocket';
    } elseif (strpos($categorySlug, "health") !== false) {
        $category = 'health';
        $category_icon = 'la-heartbeat';
    } elseif (strpos($categorySlug, "screen") !== false) {
        $category = 'screen';
        $category_icon = 'la-play-circle';
    }

    //var_dump($category);

</cms:php>

<cms:set page_title="<cms:show category_name /> | <cms:get_custom_field 'site_name' masterpage='globals.php' />" />

<cms:if "<cms:show category_desc />" >
    <cms:set page_desc="<cms:show category_desc />" />
<cms:else /> 
    <cms:set page_desc="<cms:get_custom_field 'site_desc' masterpage='globals.php' />" />
</cms:if> 

<cms:if "<cms:show category_og_image />" >
    <cms:set page_image="<cms:show category_og_image />" />
<cms:else /> 
    <cms:set page_image="<cms:get_custom_field 'og_image' masterpage='globals.php' />" />
</cms:if> 

<cms:set category="<cms:php>global $category; echo $category;</cms:php>" />
<cms:set category_icon="<cms:php>global $category_icon; echo $category_icon;</cms:php>" />