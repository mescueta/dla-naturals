<cms:template title='Gallery' clonable='1' dynamic_folders='1' gallery='1'>

   <cms:editable
      name="gg_image"
      label="Image"
      desc="Upload your main image here"
      quality='100'
      show_preview='1'
      preview_height='200'
      type="image"
   />
 
   <cms:editable
      name="gg_thumb"
      assoc_field="gg_image"
      label="Image Thumbnail"
      desc="Thumbnail of image above"
      width='80'
      height='80'
      enforce_max='1'
      type="thumbnail"
   />

   <cms:editable 
        type='relation'
        name='photo_gallery' 
        masterpage='about-commitment.php' 
        has='one' 
        no_gui='1' 
        label='-'
        quality='100'
    />

</cms:template>