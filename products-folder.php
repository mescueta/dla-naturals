<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="Products Folder" order="21" clonable="1" nested_pages='1'> 
    <cms:editable name="category_excerpt" label="Category excerpt" desc="Max of 100 characters (recommended)" type="textarea" order="1" />
    <cms:editable name="category_banner_image" type="image" label='Banner Image' desc="3333 x 1875" order="2" />
    <cms:editable name="category_og_image" type="image" label='Category OG Image (1200px X 630px)' order="3" />
    <cms:editable name="category_thumbnail_image" type="image" label='Category Thumbnail Image (500px X 540px)' order="4" />
    <cms:editable name="product_button_label" label='Button Label' type="text" order="5" />
    <cms:editable
      name='product_downloadable_file'
      label='Downloadable File'
      desc='Upload the file here'
      type='file'
      order="6"
    />

    <cms:config_form_view>
        <cms:field 'k_page_foldertitle' desc='label name' label='Label' order='0' />
        <cms:field 'k_page_foldername' skip='1'/>
    </cms:config_form_view>

    <cms:config_list_view orderby='weight' order='desc' searchable='1'>
        <cms:field 'k_selector_checkbox' />
        <cms:field 'k_page_foldertitle' sortable='0' />
        <cms:field 'k_page_foldername' />
        <cms:field 'k_up_down' header='Sort Manually' />
        <cms:field 'k_actions' />
    </cms:config_list_view>

</cms:template>

    <!-- SubNav -->
    <cms:embed 'products-list.html' />
    <!-- /SubNav -->

    <cms:if k_is_page>
    <h1>Page-view: <cms:show k_page_title /></h1>
    
    <!-- if page is contained within a folder -->
    <cms:if k_page_foldername >
        <h3>Page in folder: <cms:show k_page_foldertitle /></h3>
        
        <!-- bring the containing folder in context using cms:folders like this -->
        <cms:folders hierarchical='1' include_custom_fields='1' root=k_page_foldername depth='1'>
        
            <a href="<cms:show k_folder_link />"><cms:show k_folder_title /></a><br /> 
            
            <cms:show_repeatable 'my_gallery' >
                <cms:show my_image /> <br />
            </cms:show_repeatable>
            
        </cms:folders>
        
    </cms:if>

    </cms:if>

    <div>
      <p>asd</p>
      <cms:folders masterpage='products-item.php' >
          <cms:show k_folder_title /><cms:show k_folder_link /> <br>
      </cms:folders>
    </div>

    <!-- Footer -->
    <cms:embed 'footer.html' />
    <!-- /Footer -->    
<?php COUCH::invoke(); ?>    