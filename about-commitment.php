<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="About Us - Commitment" executable="1" order="12"> 
    <cms:editable name="page_title" label="Page Title" type="text" order="1"/>
    <cms:editable name="about_label" label="Header Label" type="text" order="2" />
    <cms:editable name="about_desc" label="Main Description" type="richtext" order="3" />
    <cms:editable name="about_other_desc" label="Other Description" type="richtext" order="4" />

    <cms:repeatable name='about_slider_images' label="Slider Images" order="5" >
       <cms:editable type='image' name='slide_image' label='Slide Image' />
    </cms:repeatable>
</cms:template>

    <cms:set page_desc="<cms:get_custom_field 'site_desc' masterpage='globals.php' />" />
    <cms:set page_image="<cms:get_custom_field 'og_image' masterpage='globals.php' />" />
    <cms:set page_title="<cms:show page_title /> | <cms:get_custom_field 'site_name' masterpage='globals.php' />" />

    <cms:embed 'header.html' />

    <!-- SubNav -->
    <cms:embed 'header-sub.html' />
    <!-- /SubNav -->
    
    <div class="main-container" id="main">

        <!-- Slider -->
        <section class="banner-section banner-secondary">
            <!-- Carousel -->
            <div class="rw carousel carousel-secondary adjusted-arrows">
                <cms:show_repeatable 'about_slider_images' >
                    <div class="cl">
                        <div class="tile tile-magazine">
                            <div class="tile-body">
                                <img src="<cms:show slide_image />" alt="About Commitment" class="thumbnail">
                            </div>
                        </div>
                    </div>
                </cms:show_repeatable>
            </div>
        </section>

        <!-- Products -->
        <section class="default-section section-80">
            <div class="rw text-left section-head">
                <div class="cl cl-8">
                    <h1 class="subtitle font-special"><cms:show about_label /></h1>
                </div>
                <div class="cl cl-12">
                    <div class="desc"><cms:show about_desc /></div>
                </div>
            </div>
            <div class="rw cl-1 text-left">
                <div class="cl other_desc">
                    <cms:show about_other_desc />
                </div> 
            </div>
        </section> 
            
    <cms:embed 'footer.html' />
    <!-- /Footer -->    

<?php COUCH::invoke(); ?>    