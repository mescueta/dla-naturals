<?php require_once( 'admindla/cms.php' ); ?> 
    <cms:template title="News - ALL" clonable="1" order="25"> 
        <cms:editable name="article_content" label='Article Content' type="richtext" order="0" />
        <cms:editable name="article_desc" label='Article Desc (100 characters)' desc="will display on the list view" type="textarea" order="1" />
        <cms:editable name="article_cover_image" label='Cover image' quality='100' crop="1" width="1200" height="630" type="image" required='1' desc='recommended size: 1200x630, for thumbnail & OG' order="2" />
    </cms:template>
    <?php
        global $articleTitle;
        $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $parts = parse_url($current_url);
        parse_str($parts['query'], $query);
    ?>
    <cms:if k_is_page>
    
    <cms:set page_title="<cms:show k_page_title /> | <cms:get_custom_field 'site_name' masterpage='globals.php' />" />
    <cms:set page_desc="<cms:show article_content />" />
    <cms:set page_image="<cms:show article_cover_image />" />

    <!-- Header -->
    <cms:embed 'header.html' />
    <!-- /Header -->

        <div class="main-container inner-page" id="main">

            <section class="default-section article-section">
                
                <div class="rw">
                   
                    <!-- Article1 -->
                    <article class="article-block cl cl-12">
                        <div class="article-header article-default">
                            <div class="article-header-content">
                                <h1><cms:show k_page_title /></h1> 
                                <time>Published on <cms:date k_page_date format='M. j, Y'/></time>
                            </div>
                        </div>
                        <div class="article-body">
                            <div class="social-media circle">
                                <ul> 
                                    <li class="facebook"><a href="#" id="shareBtn"><i class="fab fa-facebook-f"></i></a></li>
                                    <li class="twitter"><a href="http://twitter.com/share?text=<cms:show k_page_title />%20@BeeSpots&url=<?php echo $current_url ?>&hashtags=<cms:show k_page_foldername />,BeeSpots" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                </ul>
                            </div>
                            <div class="article-body-content">

                                <cms:do_shortcodes>
                                [embed code='<cms:addslashes><cms:show article_content /></cms:addslashes>']
                                </cms:do_shortcodes>


                            </div>
                        </div>
                    </article>
                    <!-- /Article1 -->
                    
                    <!-- Sidebar -->
                    <sidebar class="cl cl-4" style="display: none;">
                        
                        <div class="sidebar-header">
                            <h3>Other Updates</h3>
                        </div>
                        <div class="sidebar-body">
                           <ul>
                                <cms:pages masterpage='updates.php' limit='6' orderby='publish_date'>
                                <li><a href="<cms:show k_page_link />" title="<cms:show k_page_title />"><cms:show k_page_title /></a></li>    
                                </cms:pages>
                            </ul>
                            <a href="<cms:show k_site_link />updates.php" class="btn btn-secondary btn-sm btn-centered post-icon read-more" title="View All Articles">View All Updates<i class="icon la la-angle-right"></i></a> 
                        </div>

                        <!-- Widget - Social Media -->
                        <!-- <cms:embed 'sidebar-social-media.html' /> -->
                        <!-- /Widget - Social Media -->
                        
                    </sidebar>
                    <!-- /Sidebar -->
                    
                </div>

                
            </section>

            <script>
            document.getElementById('shareBtn').onclick = function() {
              FB.ui({
                method: 'share',
                display: 'popup',
                href: '<?php echo $current_url ?>',
              }, function(response){});
            }
            </script>
                    
    <!-- Footer -->
    <cms:embed 'footer.html' />

    <cms:else/>
        <cms:set page_title="Articles | <cms:get_custom_field 'site_name' masterpage='globals.php' />" />
        <cms:set page_desc="<cms:get_custom_field 'site_desc' masterpage='globals.php' />" />
        <cms:set page_image="<cms:get_custom_field 'og_image' masterpage='globals.php' />" />
        <cms:embed 'article-list.html'/> 
    </cms:if>
    <!-- /Footer -->    
<?php COUCH::invoke(); ?>    
