<?php require_once( 'admindla/cms.php' ); ?> 
<cms:template title="Products - ALL ITEMS" clonable="1" order="0" dynamic_folders='1' folder_masterpage='products-folder.php'> 
    <cms:editable name="product_content" label='Description' type="richtext" order="0" /> 
    <cms:editable name="product_main_image" label='Main Product Image (for thumbnail)' type="image" required='1' quality='100' order="1" desc="(1000 x 668)"/> 
    <cms:editable name="product_button_label" label='Button Label' type="text" order="2" />
    <!-- <cms:editable name="product_button_link" label='Button Link' type="text" order="3" /> -->
    <cms:editable
      name='product_downloadable_file'
      label='Downloadable File'
      desc='Upload the file here'
      type='file'
      order="3"
    />
    <cms:editable name="product_cover_image" label='Cover image (for OG image - social media sharing)' desc="(1200 x 630)" quality='100' crop="1" width="1200" height="630" type="image" order="4" />

    <cms:folder name="chocolates" title="Chocolates" order="5" /> 
    <cms:folder name="fruits" title="Fruits" />
    <cms:folder name="sugars" title="Sugars" /> 
    <cms:folder name="nuts" title="Nuts" />  

    <cms:repeatable name='product_slider_images' label="Slider Images" order="6" >
       <cms:editable type='image' name='slide_image' label='Slide Image' />
    </cms:repeatable>

    <div style="font-size: 33px; border: solid 1px red;">
        <cms:editable 
            type='reverse_relation' 
            name='details_product' 
            masterpage='product-details.php' 
            field='product_details' 
            anchor_text='Selected' 
            label='Detailed product: (table)' 
            style="font-size: 33px"
        />
    </div> 

    <cms:config_form_view>
        <cms:field 'k_page_title' desc='label name' label='Label' order='0' />
        <cms:field 'k_page_name' skip='1'/>
    </cms:config_form_view>

    <cms:config_list_view orderby='weight' order='desc' searchable='1'>

        <cms:if k_template_is_clonable = '1' >
            
            <cms:field 'k_selector_checkbox' />
            <cms:field 'k_page_title' />
            <cms:field 'k_page_foldertitle' />
            <cms:field 'k_up_down' header='Sort Manually' />
            <cms:field 'k_page_date' />
            <cms:field 'k_actions' />

        </cms:if>
        
    </cms:config_list_view>

    
</cms:template>
    <?php
        global $articleTitle;
        $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $parts = parse_url($current_url);
        parse_str($parts['query'], $query);
    ?>
    <cms:if k_is_page>
    
    <cms:set page_title="<cms:show k_page_title /> | <cms:get_custom_field 'site_name' masterpage='globals.php' />" />
    <cms:set page_desc="<cms:show article_content />" />
    <cms:set page_image="<cms:show article_cover_image />" />

    <!-- Header -->
    <cms:embed 'header.html' />
    <!-- /Header -->

        <div class="main-container" id="main">

            <!-- Banner Slider -->
            <section class="banner-section banner-secondary">
                <!-- Carousel -->
                <div class="rw carousel carousel-secondary adjusted-arrows">
                    <cms:show_repeatable 'product_slider_images' >
                    <div class="cl">
                        <div class="tile tile-magazine">
                            <div class="tile-body">
                                <img src="<cms:show slide_image />" alt="Product Images" class="thumbnail">
                            </div>
                        </div>
                    </div>
                    </cms:show_repeatable>
                </div>
            </section>

            <!-- For desktop only -->
            <div class="grid-wrapper grid-3" style="display: none;">
                <cms:show_repeatable 'product_slider_images' >
                <div class="grid-box a">
                    <div style="background-image: url(<cms:show slide_image />)" class="bg-image"></div>
                </div>
              </cms:show_repeatable>
            </div>

            <!-- Products -->
            <section class="default-section inner-product-section section-80">
                <div class="rw text-left">
                    <div class="cl cl-8">
                        <h1 class="subtitle"><cms:show k_page_title /></h1>
                    </div>
                    <div class="cl cl-4 text-right">
                        <a href="<cms:show product_downloadable_file />" class="btn btn-dark bordered-transparent" title="Download Product Sheet" target="_blank"><cms:show product_button_label /></a>
                    
                        <!-- if page is contained within a folder -->
                        <!-- <cms:if k_page_foldername >
                            <h3>Page in folder: <cms:show k_page_foldertitle /></h3>
                            
                            <cms:folders hierarchical='1' include_custom_fields='1' root=k_page_foldername depth='1'>
                            
                                <a href="<cms:show k_folder_link />"><cms:show k_folder_title /></a><br /> 
                                
                                <cms:show_repeatable 'my_gallery' >
                                    <cms:show my_image /> <br />
                                </cms:show_repeatable>
                                
                                <cms:show category_excerpt />
                                <cms:show category_excerpt />
                            </cms:folders>
                            
                        </cms:if> -->
                    </div>
                    <div class="cl cl-12">
                        <div class="desc"><cms:show product_content /></div>
                    </div>
                </div>
                
                <!-- Chocolates=0 | Fruits=1 | Sugars=2 | Nuts=3 -->

                <!-- Chocolates -->
                <cms:reverse_related_pages 'product_details' masterpage='product-details.php' custom_field='assigned_product=0'>
                <div class="rw text-left chocolates">
                    
                    <!-- With Taste Profile & Fluidity 1 -->
                    <cms:if has_tasteprofile_fluidity == '1'>
                    <div class="cl cl-12">
                        <div class="divTable responsiveTable">
                            <div class="divTableHeading">
                                <div class="divTableRow">
                                    <div class="divTableCell">Product Name <!-- / Code No / HS Code --></div>
                                    <div class="divTableCell">Cocoa bean / Origin</div>
                                    <div class="divTableCell">Taste Profile</div>
                                    <div class="divTableCell text-center">Cocoa<br>%</div>
                                    <div class="divTableCell text-center">Milk<br>%</div>
                                    <div class="divTableCell text-center">Fat<br>%</div>
                                    <div class="divTableCell text-center">Sugar<br>%</div>
                                    <div class="divTableCell">Fluidity</div>
                                </div>
                            </div>
                            <div class="divTableBody">
                                <cms:show_repeatable 'product_chocolate_table' >
                                <div class="divTableRow">
                                    <div class="divTableCell" data-title="Product Name">
                                        <div>
                                            <h3><cms:show product_chocolate_name /></h3>
                                        </div>
                                        <!-- <p>Code No.: <cms:show product_chocolate_code /></p>
                                        <p>HS CODE.: <cms:show product_chocolate_hscode /></p> -->
                                    </div>
                                    <div class="divTableCell" data-title="Cocoa bean / Origin"><div><cms:show product_chocolate_origin /></div></div>
                                    <div class="divTableCell" data-title="Taste Profile">
                                        <div class="bar-container">
                                            <div class="bar-item bar-1">
                                                <span class="bar-label">Acidity</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show acidity />0%"></span></div>
                                            </div>
                                            <div class="bar-item bar-2">
                                                <span class="bar-label">Bitter</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show bitter />0%"></span></div>
                                            </div>
                                            <div class="bar-item bar-3">
                                                <span class="bar-label">Cacao</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show cacao />0%"></span></div>
                                            </div>
                                            <div class="bar-item bar-4">
                                                <span class="bar-label">Fruity</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show fruity />0%"></span></div>
                                            </div>
                                            <div class="bar-item bar-5">
                                                <span class="bar-label">Roasted</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show roasted />0%"></span></div>
                                            </div>
                                            <div class="bar-item bar-6">
                                                <span class="bar-label">Spicy</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show spicy />0%"></span></div>
                                            </div>
                                            <div class="bar-item bar-7">
                                                <span class="bar-label">Woody</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show woody />0%"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Cocoa %">
                                        <div class="circle-progress">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_cocoa />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Milk %">
                                        <div class="circle-progress green">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_milk />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Fat %">
                                        <div class="circle-progress yellow">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_fat />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Sugar %">
                                        <div class="circle-progress yellow">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_sugar />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Fluidity">
                                        <div class="droplets-container drop<cms:show chocolate_fluidity />">
                                            <div class="droplet"></div>
                                            <div class="droplet"></div>
                                            <div class="droplet"></div>
                                            <div class="droplet"></div>
                                            <div class="droplet"></div>
                                        </div>
                                    </div>
                                </div>
                                </cms:show_repeatable>
                            </div>
                        </div>
                    </div> 
                    </cms:if>

                    <!-- With Taste Profile & Fluidity 2 -->
                    <cms:if has_tasteprofile_fluidity == '2'>
                    <div class="cl cl-12">
                        <div class="divTable responsiveTable">
                            <div class="divTableHeading">
                                <div class="divTableRow">
                                    <div class="divTableCell">Product Name <!-- / Code No / HS Code --></div>
                                    <div class="divTableCell">Cocoa bean / Origin</div>
                                    <div class="divTableCell">Taste Profile</div>
                                    <div class="divTableCell text-center">Cocoa<br>%</div>
                                    <div class="divTableCell text-center">Milk<br>%</div>
                                    <div class="divTableCell text-center">Fat<br>%</div>
                                    <div class="divTableCell text-center">Sugar<br>%</div>
                                    <div class="divTableCell">Fluidity</div>
                                </div>
                            </div>
                            <div class="divTableBody">
                                <cms:show_repeatable 'product_chocolate_table2' >
                                <div class="divTableRow" data-title="Product Name">
                                    <div class="divTableCell">
                                        <div>
                                            <h3><cms:show product_chocolate_name2 /></h3>
                                        </div>
                                        <!-- <p>Code No.: <cms:show product_chocolate_code /></p>
                                        <p>HS CODE.: <cms:show product_chocolate_hscode /></p> -->
                                    </div>
                                    <div class="divTableCell" data-title="Cocoa bean / Origin"><div><cms:show product_chocolate_origin2 /></div></div>
                                    <div class="divTableCell" data-title="Taste Profile">
                                        <div class="bar-container">
                                            <div class="bar-item bar-1">
                                                <span class="bar-label">Chocolaty</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show chocolaty />0%"></span></div>
                                            </div>
                                            <div class="bar-item bar-2">
                                                <span class="bar-label">Caramel</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show caramel />0%"></span></div>
                                            </div>
                                            <div class="bar-item bar-3">
                                                <span class="bar-label">Caramel</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show caramel />0%"></span></div>
                                            </div>
                                            <div class="bar-item bar-4">
                                                <span class="bar-label">Milky</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show milky />0%"></span></div>
                                            </div>
                                            <div class="bar-item bar-5">
                                                <span class="bar-label">Sweet</span>
                                                <div class="bar-wrapper"><span class="bar" style="width: <cms:show sweet />0%"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Cocoa %">
                                        <div class="circle-progress">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_cocoa2 />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Milk %">
                                        <div class="circle-progress green">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_milk2 />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Fat %">
                                        <div class="circle-progress yellow">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_fat2 />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Sugar %">
                                        <div class="circle-progress yellow">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_sugar2 />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Fluidity">
                                        <div class="droplets-container drop<cms:show chocolate_fluidity2 />">
                                            <div class="droplet"></div>
                                            <div class="droplet"></div>
                                            <div class="droplet"></div>
                                            <div class="droplet"></div>
                                            <div class="droplet"></div>
                                        </div>
                                    </div>
                                </div>
                                </cms:show_repeatable>
                            </div>
                        </div>
                    </div> 
                    </cms:if>

                    <!-- Without Taste Profile & Fluidity -->
                    <cms:if has_tasteprofile_fluidity == '3'>
                    <div class="cl cl-12">
                        <div class="divTable responsiveTable">
                            <div class="divTableHeading">
                                <div class="divTableRow">
                                    <div class="divTableCell">Product Name <!-- / Code No / HS Code --></div>
                                    <div class="divTableCell">Cocoa bean / Origin</div>
                                    <div class="divTableCell text-center">Cocoa<br>%</div>
                                    <div class="divTableCell text-center">Milk<br>%</div>
                                    <div class="divTableCell text-center">Fat<br>%</div>
                                    <div class="divTableCell text-center">Sugar<br>%</div>
                                </div>
                            </div>
                            <div class="divTableBody">
                                <cms:show_repeatable 'product_chocolate_table3' >
                                <div class="divTableRow">
                                    <div class="divTableCell" data-title="Product Name">
                                        <div><h3><cms:show product_chocolate_name3 /></h3></div>
                                    </div>
                                    <div class="divTableCell" data-title="Cocoa bean / Origin"><div><cms:show product_chocolate_origin3 /></div></div>
                                    <div class="divTableCell" data-title="Cocoa %">
                                        <div class="circle-progress">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_cocoa3 />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Milk %">
                                        <div class="circle-progress green">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_milk3 />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Fat %">
                                        <div class="circle-progress yellow">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_fat3 />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Sugar %">
                                        <div class="circle-progress yellow">
                                            <div class="circular-progress circular-progress<cms:show product_chocolate_sugar3 />"></div>
                                        </div>
                                    </div>
                                </div>
                                </cms:show_repeatable>
                            </div>
                        </div>
                    </div> 
                    </cms:if>

                </div>
                </cms:reverse_related_pages>

                <!-- Fruits -->
                <cms:reverse_related_pages 'product_details' masterpage='product-details.php' custom_field='assigned_product=1'>
                <div class="rw chart-container fruits">
                    <div class="cl cl-3">
                        <label>Fruit %</label>
                        <div class="circle-progress orange inline">
                            <div class="circular-progress circular-progress<cms:show product_fruit_percentage />"></div>
                        </div>
                    </div>
                    <div class="cl cl-3">
                        <label>Bake stability</label>
                        <div class="circle-progress orange">
                            <div class="circular-progress circular-progress<cms:show product_fruit_bake_percentage />"></div>
                        </div>
                    </div>
                    <div class="cl cl-3">
                        <label>Fruit Integrity</label>
                        <div class="circle-progress orange">
                            <div class="circular-progress circular-progress<cms:show product_fruit_integrity_percentage />"></div>
                        </div>
                    </div>
                    <div class="cl cl-3">
                        <label>Sugar</label>
                        <div class="circle-progress orange">
                            <div class="circular-progress circular-progress<cms:show product_fruit_sugar_percentage />"></div>
                        </div>
                    </div>
                </div>
                <div class="rw text-left fruits">
                    <div class="cl cl-12">
                        <div class="divTable responsiveTable">
                            <div class="divTableHeading">
                                <div class="divTableRow">
                                    <div class="divTableCell">Flavor</div>
                                    <div class="divTableCell">Origin of Fruit / Ingredient</div>
                                    <div class="divTableCell">Form of Fruits</div>
                                </div>
                            </div>
                            <div class="divTableBody">
                                <cms:show_repeatable 'product_fruits_table' >
                                <div class="divTableRow">
                                    <div class="divTableCell" data-title="Flavor">
                                        <div><h3><cms:show product_fruits_flavor /></h3></div>
                                    </div>
                                    <div class="divTableCell" data-title="Origin of Fruit / Ingredient">
                                        <div class="thumb-desc circle">
                                            <div class="img-wrapper">
                                                <img src="<cms:show product_fruits_origin_image />" alt="Tile here">
                                            </div>
                                            <label><cms:show product_fruits_origin /></label>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Form of Fruits">
                                        <div>
                                            <p><cms:show product_fruits_form /></p>
                                        </div>
                                    </div>
                                </div>
                                </cms:show_repeatable>
                            </div>
                        </div>
                        <!-- DivTable.com -->
                    </div>  
                </div>
                </cms:reverse_related_pages>


                <!-- Sugars -->
                <cms:reverse_related_pages 'product_details' masterpage='product-details.php' custom_field='assigned_product=2'>
                <div class="rw text-left sugars">
                    <div class="cl cl-12">
                        <div class="divTable responsiveTable">
                            <div class="divTableHeading">
                                <div class="divTableRow">
                                    <div class="divTableCell">Color / Code No / HS Code</div>
                                    <div class="divTableCell"></div>
                                </div>
                            </div>
                            <div class="divTableBody">
                                <cms:show_repeatable 'product_sugars_table' >
                                <div class="divTableRow">
                                    <div class="divTableCell" data-title="Color / Code No / HS Code">
                                        <div>
                                            <h3><cms:show product_sugars_name /></h3>
                                        </div>
                                        <!-- <p>Code No.: CVDK5501-15</p>
                                        <p>HS CODE.: 1806.20.10</p> -->
                                    </div>
                                    <div class="divTableCell" data-title="">
                                        <div class="thumb-desc circle">
                                            <div class="img-wrapper">
                                                <img src="<cms:show product_sugars_colors_flavors />" alt="<cms:show product_sugars_name />">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </cms:show_repeatable>
                            </div>
                        </div>
                    </div>  
                </div>
                </cms:reverse_related_pages>


                <!-- Nuts -->
                <cms:reverse_related_pages 'product_details' masterpage='product-details.php' custom_field='assigned_product=3'>
                <div class="rw text-left nuts">
                    <div class="cl cl-12">
                        <div class="divTable responsiveTable">
                            <div class="divTableHeading">
                                <div class="divTableRow">
                                    <div class="divTableCell">Flavor / Code No / HS Code</div>
                                    <div class="divTableCell text-center">Hazelnut <br>%</div>
                                    <div class="divTableCell text-center">Sugar <br>%</div>
                                </div>
                            </div>
                            <div class="divTableBody">
                                <cms:show_repeatable 'product_nuts_table' >
                                <div class="divTableRow">
                                    <div class="divTableCell" data-title="Flavor / Code No / HS Code">
                                        <div>
                                            <h3><cms:show product_nuts_name /></h3>  
                                        </div>
                                        <!-- <p>Code No.: CVDK5501-15</p>
                                        <p>HS CODE.: 1806.20.10</p> -->
                                    </div>
                                    <div class="divTableCell" data-title="Hazelnut %">
                                        <div class="circle-progress">
                                            <div class="circular-progress circular-progress<cms:show product_nuts_hazelnut />"></div>
                                        </div>
                                    </div>
                                    <div class="divTableCell" data-title="Sugar %">
                                        <div class="circle-progress yellow">
                                            <div class="circular-progress circular-progress<cms:show product_nuts_sugar />"></div>
                                        </div>
                                    </div>
                                </div>
                                </cms:show_repeatable>
                            </div>
                        </div>
                    </div>  
                </div>
                </cms:reverse_related_pages>

            </section>   

                
            <!-- Products -->
            <section class="default-section products-section section-80 section-max"">
                <h4 class="title">You might also be interested in:</h4>
                <div class="rw cl-4 carousel carousel-default">
                    <cms:pages masterpage='products-item.php' folder="<cms:show k_page_foldername/>" limit="10">
                    <div class="cl">
                        <div class="tile tile-plain tile-clickable">
                            <div class="tile-body">
                                <img src="<cms:show product_main_image />" alt="<cms:show k_page_title />" class="thumbnail">
                            </div>
                            <div class="tile-footer">
                                <h2><cms:show k_page_title /></h2>
                                <cms:excerptHTML count="14" ignore="img, table, br, iframe"><p><cms:show product_content /></p></cms:excerptHTML>
                            </div>
                            <a href="<cms:show k_page_link />" title="<cms:show k_page_title />" class="wrapped-link"></a>
                        </div>
                    </div>
                    </cms:pages>
                </div>   
            </section>
                    
    <!-- Footer -->
    <cms:embed 'footer.html' />

    <cms:else/>
        <cms:set page_title="All Products | <cms:get_custom_field 'site_name' masterpage='globals.php' />" />
        <cms:set page_desc="<cms:get_custom_field 'site_desc' masterpage='globals.php' />" />
        <cms:set page_image="<cms:get_custom_field 'og_image' masterpage='globals.php' />" />
        <cms:embed 'products-list.html'/>
        <cms:embed 'footer.html' /> 
    </cms:if>
    <!-- /Footer -->    
<?php COUCH::invoke(); ?>    
