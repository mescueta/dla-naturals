<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="Homepage - BREAKER" executable="0" order="3" > 
    <cms:editable name="breaker_title" label="Breaker title" type="text" />
    <cms:editable name="breaker_desc" label="Breaker description" type="richtext" />
    <cms:editable name="breaker_button_label" label="Button's Label" type="text" />
    <cms:editable name="breaker_button_link" label="Button's link" type="text"/>
    <cms:editable name="breaker_cover_image" type="image" label='Cover Image (1280px X 400px)' quality='100' />
    <cms:editable name="breaker_cover_image" type="image" label='Background Image' quality='100' desc='Upload video here (1280px X 400px)' />
    <cms:editable name='breaker_video' label='Background Video' desc='Upload video here (mp4)' type='file' />

    <cms:editable name="breaker_background" label="Select the background" desc="Select the parent menu to display this item" opt_values='Image=0 | Video=1' type='dropdown' />
</cms:template>
<?php COUCH::invoke(); ?>