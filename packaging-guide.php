<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="Packaging guide - DETAILS" order="9"> 
    <cms:editable name="packaging_name" label="Packaging name" type="text" order="0" />
    <cms:editable name="packaging_desc" label="Packaging description" type="richtext" order="1" />
    <cms:editable name="packaging_excerpt" label="Packaging excerpt" desc="Max of 100 characters (recommended)" type="textarea" order="2" />
    <cms:editable name="packaging_banner_image" type="image" label='Banner Image' desc="3333 x 1875" order="3" />
    <cms:editable name="packaging_og_image" type="image" label='Packaging OG Image (1200px X 630px)' order="4" />
    <cms:editable name="packaging_thumbnail_image" type="image" label='Packaging Thumbnail Image (500px X 540px)' order="5" />
    <cms:editable name="packaging_button_label" label='Button Label' type="text" order="6" />

    <cms:editable
      name='packaging_downloadable_file'
      label='Downloadable File'
      desc='Upload the file here'
      type='file'
      order="8"
    />
</cms:template>

    <!-- SubNav -->
    <cms:embed 'packaging-list.html' />
    <!-- /SubNav -->

    <!-- Footer -->
    <cms:embed 'footer.html' />
    <!-- /Footer -->    
<?php COUCH::invoke(); ?>    