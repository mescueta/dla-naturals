<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="About Us - Fine Ingredients" executable="1" order="14"> 
    <cms:editable name="page_title" label="Page Title" type="text" order="0"/>
    <cms:editable name="about_label" label="Header Label" type="text" order="1" />
    <cms:editable name="about_desc" label="Main Description" type="richtext" order="2" />
    <cms:editable name="about_other_desc" label="Other Description" type="richtext" order="3" />
    <cms:editable type='image' name='banner_image' label='Banner Image' order="4" />
</cms:template>

    <cms:set page_desc="<cms:get_custom_field 'site_desc' masterpage='globals.php' />" />
    <cms:set page_image="<cms:get_custom_field 'og_image' masterpage='globals.php' />" />
    <cms:set page_title="<cms:show page_title /> | <cms:get_custom_field 'site_name' masterpage='globals.php' />" />

    <cms:embed 'header.html' />

    <!-- SubNav -->
    <cms:embed 'header-sub.html' />
    <!-- /SubNav -->
    
    <div class="main-container" id="main">

        <!-- Details -->
        <section class="default-section full-img-section">
            <div class="rw">
                <div class="cl img-container">
                    <img src="<cms:show banner_image />" alt="About Ingredients" class="main-img">
                </div>
            </div>
            <div class="rw rw-80 text-left section-head">
                <div class="cl">
                    <h1 class="subtitle font-special"><cms:show about_label /></h1>
                    <div class="desc"><cms:show about_desc /></div>
                    <div class="other_desc"><cms:show about_other_desc /></div>
                </div>
            </div>   
        </section>
        <!-- /Details -->
            
    <cms:embed 'footer.html' />
    <!-- /Footer -->    

<?php COUCH::invoke(); ?>    