<?php require_once( 'admindla/cms.php' ); ?>
    <cms:template title="Search Results" order="25" hidden="1"> 
    </cms:template>
    <?php
        $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $parts = parse_url($current_url);
        parse_str($parts['query'], $query);

        //URL Param
        $queryText = str_replace(' ', '%20', $query['s']);
        $queryTextOutput = $query['s'];
    ?>

    <!-- Header -->
    <cms:embed 'header.html' />
    <!-- /Header -->

        <div class="main-container inner-page" id="main">
        
        <!-- Search Results -->
        <section class="default-section search-results">
            <div class="container">
                <div class="rw rw-full cl-4">
                    <cms:search masterpage='products-item.php, product-details.php' limit='8' keywords="<cms:gpc 's' />" >
                    <cms:if k_paginated_top >
                        <h1 class="header-title"><cms:show k_total_records /> results match <em>"<?php echo $queryTextOutput; ?>"</em></h1>
                        <div class="search-results-details cl cl-12">
                            <cms:if k_paginator_required >
                                (Page <cms:show k_current_page /> of <cms:show k_total_pages />)&nbsp;
                            </cms:if>
                            <!-- <cms:show k_total_records /> matches found - -->
                            Displaying <cms:show k_record_from />-<cms:show k_record_to />
                        </div>
                    </cms:if>

                    <cms:no_results>
                        <h1 class="header-title">No results match for <em>"<cms:show k_search_query />"</em></h1>
                    </cms:no_results>

                    <div class="cl">
                        <div class="tile tile-plain tile-clickable">
                            <div class="tile-body">
                                <cms:set product_image='<cms:show product_main_image />' scope='global'/>
                                <cms:if product_image == '' >
                                    <img src="<cms:get_custom_field 'product_main_image' masterpage='products-item.php' />" alt="<cms:show k_page_title />" class="thumbnail aa" onerror="this.style.opacity='0'">
                                <cms:else />
                                   <img src="<cms:show product_main_image />" alt="<cms:show k_page_title />" class="thumbnail bb" onerror="this.style.opacity='0'">
                                </cms:if>
                            </div>
                            <div class="tile-footer">
                                <h2><cms:show k_page_title /></h2>
                                <cms:excerptHTML count="14" ignore="img, table, br, iframe"><cms:show k_search_content /></cms:excerptHTML>
                            </div>
                            <cms:if product_image == '' >
                                <a href="<cms:show k_site_link />products-item/<cms:show k_page_name />.html" title="<cms:show k_page_title />" class="wrapped-link a"></a>
                            <cms:else />
                               <a href="<cms:show k_site_link />products-item/<cms:show k_page_name />.html" title="<cms:show k_page_title />" class="wrapped-link b"></a>
                            </cms:if>
                        </div>
                    </div>

                    <cms:paginator />
                    
                    </cms:search>
                </div>  

            </div> 
        </section>
        <!-- /Search Results -->

        <!-- Products -->
        <section class="default-section products-section section-80 section-max"">
            <h4 class="title">You might also be interested in:</h4>
            <div class="rw cl-4 carousel carousel-default">
                <cms:pages masterpage='products-item.php' folder="all" limit="10">
                <div class="cl">
                    <div class="tile tile-plain tile-clickable">
                        <div class="tile-body">
                            <img src="<cms:show product_main_image />" alt="<cms:show k_page_title />" class="thumbnail">
                        </div>
                        <div class="tile-footer">
                            <h2><cms:show k_page_title /></h2>
                            <cms:excerptHTML count="14" ignore="img, table, br, iframe"><p><cms:show product_content /></p></cms:excerptHTML>
                        </div>
                        <a href="<cms:show k_page_link />" title="<cms:show k_page_title />" class="wrapped-link"></a>
                    </div>
                </div>
                </cms:pages>
            </div>   
        </section>
                    
    <!-- Footer -->
    <cms:embed 'footer.html' />
    <!-- /Footer -->    
<?php COUCH::invoke(); ?>    