<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="Homepage - MAIN SLIDE" clonable="1" executable="0" order="2"> 
    <cms:editable name='slider_cover' label='Image Cover Photo' desc='Banner Image (1140 × 550)' show_preview='1' type='image' /> 
    <cms:editable name='slider_content' label='Content (image reco width:  800px)' type='richtext' /> 
    <cms:editable name='slider_button_label' label='Button Label' type='text' />
    <cms:editable name='slider_button_link' label='Button Link' type='text' />

	<cms:editable name='slider_video' label='Slider Video' desc='Upload video here (mp4)' type='file' />

    <cms:editable name="slider_background" label="Select the slider background type" desc="Image/Video" opt_values='Image=0 | Video=1' type='dropdown' />

    <cms:config_list_view orderby='weight' order='desc'>
        <cms:field 'k_selector_checkbox' />
        <cms:field 'k_page_title' sortable='0' />
        <cms:field 'k_up_down' header='Sort Manually' />
        <cms:field 'k_actions' />
    </cms:config_list_view>
</cms:template>
<?php COUCH::invoke(); ?>