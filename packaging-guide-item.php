<?php require_once( 'admindla/cms.php' ); ?> 
<cms:template title="Packaging Guide - ALL ITEMS" clonable="1" order="10"> 
    <cms:editable name="packaging_main_image" label='Main Packaging Image (for thumbnail)' type="image" required='1' quality='100' order="0" desc="(500 x 500)"/> 
    <cms:editable name="packaging_content" label='Details' type="richtext" order="1" />

    <cms:config_form_view>
        <cms:field 'k_page_title' desc='label name' label='Label' order='0' />
        <cms:field 'k_page_name' skip='1'/>
    </cms:config_form_view>

    <cms:config_list_view orderby='weight' order='desc' searchable='1'>
        <cms:field 'k_selector_checkbox' />
        <cms:field 'k_page_title' sortable='0' />
        <cms:field 'k_up_down' header='Sort Manually' />
        <cms:field 'k_actions' />
    </cms:config_list_view>

</cms:template>
    <?php
        global $articleTitle;
        $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $parts = parse_url($current_url);
        parse_str($parts['query'], $query);
    ?>
    <cms:if k_is_page>
    
    <cms:set page_title="<cms:show k_page_title /> | <cms:get_custom_field 'site_name' masterpage='globals.php' />" />
    <cms:set page_desc="<cms:show packaging_content />" />
    <cms:set page_image="<cms:show packaging_main_image />" />

    <!-- Header -->
    <cms:embed 'header.html' />
    <!-- /Header -->

        <div class="main-container" id="main">


                    
    <!-- Footer -->
    <cms:embed 'footer.html' />

    <cms:else/>
        <cms:set page_title="All Packaging guide | <cms:get_custom_field 'site_name' masterpage='globals.php' />" />
        <cms:set page_desc="<cms:get_custom_field 'site_desc' masterpage='globals.php' />" />
        <cms:set page_image="<cms:get_custom_field 'og_image' masterpage='globals.php' />" />
        <cms:embed 'packaging-list.html'/> 
    </cms:if>
    <!-- /Footer -->    
<?php COUCH::invoke(); ?>    
