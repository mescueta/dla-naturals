<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="Footer - Links" clonable="1" executable="0"> 
    <cms:editable name='footer_link' label='Link' type='text' />

    <cms:config_form_view>
        <cms:field 'k_page_title' desc='Label' label='Label' order='0' />
        <cms:field 'k_page_name' skip='1'/>
    </cms:config_form_view>

    <cms:config_list_view orderby='weight' order='desc'>
        <cms:field 'k_page_title' sortable='0' />
        <cms:field 'k_up_down' header='Sort Manually' />
        <cms:field 'k_actions' />
    </cms:config_list_view>
</cms:template>
<?php COUCH::invoke(); ?>