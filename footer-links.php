<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="Footer - LINK" clonable="1" executable="1"  order="25"> 

    <cms:editable name='footer_link' label='Link' type='text' />

    <cms:editable
      name="assigned_menu"
      label="Parent Menu"
      desc="Select the parent menu to display this item"
      opt_values='Our Company=0 | Our Products=1 | Resources=2'
      type='dropdown'
    />

    <cms:editable
      name="status"
      label="Status"
      desc="Show/Hide link"
      opt_values='Disable=0 | Enable=1'
      type='dropdown'
    />

    <!-- <cms:folder name="our_company" title="Our Company" /> 
    <cms:folder name="our_products" title="Our Products" />
    <cms:folder name="resources" title="Resources" /> --> 

    <cms:config_form_view>
        <cms:field 'k_page_title' desc='label name' label='Label' order='0' />
        <cms:field 'k_page_name' skip='1'/>
    </cms:config_form_view>

    <cms:config_list_view orderby='weight' order='desc'>
        <cms:field 'k_selector_checkbox' />
        <cms:field 'k_page_title' sortable='0' />
        <cms:field 'k_up_down' header='Sort Manually' />
        <cms:field 'k_actions' />
    </cms:config_list_view>
</cms:template>
<?php COUCH::invoke(); ?>