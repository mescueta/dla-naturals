/*!
 * FED DLA Naturals
 * 2018 | DLA
 */

DLA = {

    init: function() {
        DLA.globalFunction();
        DLA.customSelectFunction();
        DLA.dropdownFunction();
        DLA.ssoFormFunction();
        DLA.formFunction();
        DLA.slickSamples();
    },

    globalFunction: function() {

        setTimeout(function() {
            $('.loader-container').fadeOut('fast');
        }, 1500);

        //Collapsible
        var allCollapsible = $('.has-submenu .submenu a');
        $(document).on('click', 'li.has-submenu a', function(e) {
            $(this).parent().find("ul.submenu").slideToggle();
            $(this).parent().toggleClass("opened");
        });

        //Burger Nav toggle
        $('.burgernav, .close-menu').click(function(e) {
            $('.burgernav').toggleClass('open');
            $('header, body').toggleClass('nav-open');
            e.preventDefault();
        });

        //Modal target
        $(document).on('click', '[data-toggle="lightbox"], [data-toggle="modal"]', function(e) {
            e.preventDefault();
            var target = $(this).data("target");
            $(target).fadeIn('fast');  
            $('body').addClass('lightbox-open'); //used to disable scroll outside
        });

        //Modal Close [Custom]
        $(document).on('click', '[data-toggle="close"]', function(e) {
            e.preventDefault();  
            $(this).closest('.lightbox').fadeOut();
            $('body').removeClass('lightbox-open'); 
        });

        //Search (Global)
        $(document).on('click', '.form-search label.active', function(e) {
            e.preventDefault();  
            var pageUrl = '/search.php?s=', 
                searchInput = $(".form-search input[type='text']").val();
            location.replace(pageUrl + searchInput);
        });


        //Search - on click
        $(document).on('click', '.search-icon-container', function(e) {
            e.preventDefault();
            var target = $(this).data("target");
            $(target).fadeIn('fast');  
            $('body').addClass('lightbox-open'); //used to disable scroll outside
            $('.lightbox-search input[type="text"]').focus();
            return false;
        });

        //Search (Global) - on hover/click
        $(document).on('click', '.form-search .la-search', function(e) {
            //alert(1);   
            $('.form-search label').click();
            $('.form-group.form-search').addClass("active");
            
        }); 

        //LIGHTBOX - Close
        $(document).on('click', '.lightbox .btn-close', function(event) {
            event.preventDefault();
            $('body').removeClass('lightbox-open');
            setTimeout(function() {
                $('.lightbox').fadeOut('fast');
            }, 100);
        });

        //Search (Global) - on enter key
        $(".form-search input[type='text']").keypress(function(e) {
            var key = e.which;
            if (key == 13) { // the enter key code
                $('.form-search label.active').click();
                return false;
            }
        });

        //Header Search Icon
        $(document).on('click', '.nav-right .btn-search', function(e) {
            e.preventDefault();
            var target = $(this).data("target");
            $(target).fadeIn('fast');
            $('body').addClass('lightbox-open'); //used to disable scroll outside
            $(".lightbox-search .form-control").focus();
        });


        $(window).scroll(function() {

            if ($('.banner-block').length > 0) {

                var article_block = $('.banner-block');
                var position = article_block.position().top - $(window).scrollTop();

                if (position <= 10) {
                    article_block.addClass('bg-fixed');
                } else {
                    article_block.removeClass('bg-fixed');
                }

            }

        });


    }, 

    customSelectFunction: function() {
        // var allOptions = $("ul").children('li:not(.custom-selected)');
        $(document).on("click", ".select-items > div", function() {
            $(this).parent().parent().find(".select-selected").text($(this).text());
        });
    },

    dropdownFunction: function() {
        var x, i, j, selElmnt, a, b, c;
        /*look for any elements with the class "custom-select":*/
        x = document.getElementsByClassName("custom-select");
        for (i = 0; i < x.length; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            /*for each element, create a new DIV that will act as the selected item:*/
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x[i].appendChild(a);
            /*for each element, create a new DIV that will contain the option list:*/
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 0; j < selElmnt.length; j++) {
                /*for each option in the original select element,
                create a new DIV that will act as an option item:*/
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;

                c_showSlug = selElmnt.options[j].value;
                c_categorySlug = selElmnt.options[j].id;
                c.setAttribute("ng-reflect-router-link", c_categorySlug + '/' + c_showSlug);

                c.addEventListener("click", function(e) {
                    /*when an item is clicked, update the original select box,
                    and the selected item:*/
                    var y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            // a.addEventListener("click", function(e) {
            //     /*when the select box is clicked, close any other select boxes,
            //     and open/close the current select box:*/
            //     e.stopPropagation();
            //     closeAllSelect(this);
            //     this.nextSibling.classList.toggle("select-hide");
            //     this.classList.toggle("select-arrow-active");
            // });
        }

        function closeAllSelect(elmnt) {
            /*a function that will close all select boxes in the document,
            except the current select box:*/
            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            for (i = 0; i < y.length; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i);
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }
        /*if the user clicks anywhere outside the select box,
        then close all select boxes:*/
        document.addEventListener("click", closeAllSelect);
    },

    ssoFormFunction: function() {

        $(".animated .form-control").focus(function() {
            $(this).parent().addClass("active");
        }).blur(function() {
            if ($(this).val() != "") {
                $(this).parent().addClass("active");
            } else {
                $(this).parent().removeClass("active");
            }
        }); 

        //Toggle Password
        // $(document).on("click", ".toggle-show-password", function() {
        //     $(this).toggleClass("fa-eye fa-eye-slash");
        //     //$(this).toggleAttr('type', "text", "password");
        //     var t = $(this).parent().find(".form-control");
        //     $(this).hasClass("fa-eye") ? t.attr("type", "text") : t.attr("type", "password");
        // });

    },

    formFunction: function() {
        var input_selector = 'input[type=text], input[type=password], input[type=email], input[type=url], input[type=tel], input[type=number], input[type=search], input[type=date], input[type=time], textarea';
        if ($("input[type=text]").is(':focus')) {
            $this.siblings('label').addClass('active');
        }
        $(input_selector).each(function(element, index) {
            var $this = $(this);
            if ($this.attr('placeholder') != undefined || $this.val().length > 0) {
                $this.siblings('label').addClass('active');
            } else {
                $this.siblings('label').removeClass('active');
            }
        });

        /**
         * Add active when element has focus
         * @param {Event} e
         */
        document.addEventListener('focus', function(e) {
            if ($(e.target).is(input_selector)) {
                $(e.target).siblings('label, .prefix').addClass('active');
            }
        }, true);

        /**
         * Remove active when element is blurred
         * @param {Event} e
         */
        document.addEventListener('blur', function(e) {
            var $inputElement = $(e.target);
            if ($inputElement.is(input_selector)) {
                var selector = '.prefix';

                if ($inputElement[0].value.length === 0 && $inputElement[0].validity.badInput !== true && $inputElement.attr('placeholder') === undefined) {
                    selector += ', label';
                }
                $inputElement.siblings(selector).removeClass('active');
                //M.validate_field($inputElement);
            }
        }, true);

    },

    slickSamples: function() {
        if ($(".carousel-default").length > 0) {
            $('.carousel-default').slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: false
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }

        if ($(".carousel-secondary").length > 0) {
            
            if ($('.carousel-secondary .cl').length == 1) {
                $('.carousel-secondary').slick({
                    dots: false,
                    infinite: true,
                    speed: 300,
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false
                });
            } else {
                $('.carousel-secondary').slick({
                    dots: false,
                    infinite: true,
                    speed: 300,
                    arrows: false,
                    slidesToShow: 1.67,
                    slidesToScroll: 1.67,
                    centerMode: true,
                    responsive: [{
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 1.67,
                                slidesToScroll: 1.67,
                                infinite: true,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
            }
        }

        if ($(".carousel-primary").length > 0) {
            $('.carousel-primary').slick({
                dots: true,
                infinite: true,
                speed: 900,
                fade: true,
                draggable: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: true,
                cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
                touchThreshold: 100,
                autoplay: true,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }

        if ($(".carousel-dots").length > 0) {
            $('.carousel-dots').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }

        if ($(".carousel-banner-special").length > 0) {
            var $slick = $('.carousel-banner-special'),
                $progressBar = $('.slider-progress'),
                $progressBarLabel = $('.progress');

            //Add slide count for progressbar  
            $slick.on('init', function(event, slick) {
                $progressBar.addClass("slide-" + slick.slideCount);
            });

            $slick.slick({
                dots: true,
                slide: ".cl",
                autoplay: true,
                autoplaySpeed: 5000,
                pauseOnDotsHover: true,
                infinite: true,
                arrows: false,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [{
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 568,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

            $slick.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                var slideCountPercentage = 100 / slick.slideCount;
                var calc = (slideCountPercentage * nextSlide) + slideCountPercentage;
                $progressBarLabel.text(calc + '% completed');
                $progressBarLabel.css({
                    width: calc + "%"
                });
            });

        }

        if ($(".carousel-banner").length > 0) {

            $('.carousel-banner.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.carousel-banner.slider-nav'
            });
            $('.carousel-banner.slider-nav').slick({
                slidesToShow: 10,
                slidesToScroll: 1,
                asNavFor: '.carousel-banner.slider-for',
                dots: false,
                arrows: true,
                centerMode: true,
                focusOnSelect: true
            });

        }

        if ($(".carousel-banner-custom").length > 0) {

            $('.carousel-banner-custom.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.carousel-banner-custom.slider-nav'
            });
            $('.carousel-banner-custom.slider-nav').slick({
                slidesToShow: 10,
                slidesToScroll: 1,
                asNavFor: '.carousel-banner-custom.slider-for',
                dots: false,
                arrows: true,
                centerMode: false,
                focusOnSelect: true
            });

        }

        if ($(".carousel-two-slides").length > 0) {
            $('.carousel-two-slides.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.carousel-two-slides.slider-nav'
            });
            $('.carousel-two-slides.slider-nav').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                asNavFor: '.carousel-two-slides.slider-for',
                dots: false,
                arrows: true,
                centerMode: false,
                focusOnSelect: true
            });
        } 

        if ($(".carousel-video").length > 0) {
            $('.carousel-video').slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }, 
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }
    }

};

$(document).ready(function() {
    DLA.init();
});