<?php 

  $FUNCS->register_shortcode( 'googlemap', 'googlemap_handler' );

  function googlemap_handler( $params, $content=null ){
      global $FUNCS; 

      extract( $FUNCS->get_named_vars(array(
         'src' => ''
      ), $params) );

      return $src;
  }

    // All-Purpose Embed Shortcode
    // Embed any code (almost).
    // Careful of your quotation mark types.
    // Won't accept the word "script."  No new lines. PHP code won't work.
    // Usage: [embed code='<p>Any code goes here.</p>']
    $FUNCS->register_shortcode( 'embed', 'embed_handler' );
    function embed_handler( $params, $content=null ){
      global $FUNCS;

      extract( $FUNCS->get_named_vars(array( 
         'code' => '',
      ), $params) );

      // Sanitize parameters
      //$code = htmlspecialchars( $code, ENT_QUOTES );

       // Pass on the code to Couch for execution using the 'embed' function
      return $FUNCS->embed( $code, $is_code=1 );
    }

//     $FUNCS->register_tag( 'trim', array('CustomTags', 'trim_all') );
// class CustomTags{
   
//     function trim_all( $str, $node ){

//         $res = $str[0]['rhs'];
//         return trim( preg_replace( "/\s+/" , ' ' , $res ) , " \t\n\r\0\x0B" );
//     }
   
// } 

?>


