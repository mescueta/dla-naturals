<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="About Us - Tradition" executable='1' order="11"> 
    <cms:editable name="page_title" label="Page Title" type="text" order="0"/>
    <cms:editable name="about_desc" label="Description" type="richtext" order="1"/>
    <cms:editable name="about_center_image" type="image" label='Top Image' order="2" />
    <cms:editable name="about_bg_image" type="image" label='Background Image (3333px X 1875px)' order="3" />
</cms:template>

    <cms:set page_desc="<cms:get_custom_field 'site_desc' masterpage='globals.php' />" />
    <cms:set page_image="<cms:get_custom_field 'og_image' masterpage='globals.php' />" />
    <cms:set page_title="<cms:show page_title /> | <cms:get_custom_field 'site_name' masterpage='globals.php' />" />

    <cms:embed 'header.html' />

    <!-- SubNav -->
    <cms:embed 'header-sub.html' />
    <!-- /SubNav -->
    
    <div class="main-container" id="main">

            <!-- About  -->
            <section class="banner-section banner-default centered">
                <!-- Carousel -->
                <div class="rw">
                    <div class="cl">
                        <div class="tile tile-magazine">
                            <div class="tile-body">
                                <img src="<cms:show about_bg_image />" alt="About" class="thumbnail">
                                <div class="floated-content">
                                    <img src="<cms:show about_center_image />" alt="About">
                                    <!-- <cms:show page_title /> -->
                                    <cms:show about_desc />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
    <cms:embed 'footer.html' />
    <!-- /Footer -->    

<?php COUCH::invoke(); ?>    