<?php require_once( 'admindla/cms.php' ); ?>
<?php require_once( 'MailChimp.php' ); ?>

<cms:template title='Newsletter Settings' order="25">
    <cms:editable label='MailChimp API Key' name='mc_api_key' required='1' type='text'/>
    <cms:editable label='MailChimp List ID' name='mc_list_id' required='1' type='text'/>
</cms:template>

<!-- Header -->
<cms:embed 'header.html' />
<!-- /Header -->

<div class="main-container" id="main">

    <section class="default-section section-80 subscription-section">

    <cms:form method='post' name='subscribe'>
        <cms:if k_success>
            <cms:php>
                global $CTX;
                use \DrewM\MailChimp\MailChimp;
                $MailChimp = new MailChimp($CTX->get('mc_api_key'));

                $result = $MailChimp->post('lists/' . $CTX->get('mc_list_id') . '/members', array(
                    'email_address' => $CTX->get('frm_email'),
                    'status'        => 'pending'
                ));

                if (isset($result['id'])) {
                    echo "<p class=\"success\"><strong>Thank you for your subscribing!</strong><br>Please check your inbox for a confirmation email.</p>";
                } else if (isset($result['type'])) {
                    //echo "<p class=\"error\"><strong>" . $result['title'] . " (" . $result['status'] . "):</strong> " . $result['detail'] . "<br><br>" . $result['type'] . "</p>";
                    echo "<p class=\"error\"><strong>Error: " . $result['title'] . ".</strong><br> " . $result['detail'] . "<br><br>" ."</p>";
                } else {
                    echo "<p class=\"error\">An unknown error was encountered. Please try again later or contact us.</p>";
                }
            </cms:php>
        <cms:else/>
            <cms:if k_error>
                <div class="notice">Please enter a valid email address.</div>
            </cms:if>

            <div class="form-group cl signup-form">
                <cms:input name='email' required='1' type='text' validator='email' id="email" class="validate"/>
                <label for="email">Email Address</label>
                <button type="submit" class="btn btn-default btn-block">Subscribe to Newsletter &nbsp;<i class="icon far fa-envelope"></i></button>
            </div>

        </cms:if>

        
    </cms:form>

    </section>

    <!-- Products -->
    <section class="default-section products-section section-80 section-max padded-lg" id="products_section">
        <h1 class="section-title">Our products</h1>
        <div class="rw cl-4">
            <cms:pages masterpage='products-chocolates.php'>
            <div class="cl">
                <div class="tile tile-plain tile-clickable">
                    <div class="tile-body">
                        <img src="<cms:show category_thumbnail_image />" alt="Tile here" class="thumbnail">
                    </div>
                    <div class="tile-footer">
                        <h2><cms:show category_name /></h2>
                        <cms:excerptHTML count="14" ignore="img, table, br, h1, h2, iframe"><p><cms:show category_excerpt /></p></cms:excerptHTML>
                    </div>
                    <a href="<cms:show k_page_link />" title="<cms:show k_page_title />" class="wrapped-link"></a>
                </div>
            </div>
            </cms:pages>
            <cms:pages masterpage='products-fruits.php'>
            <div class="cl">
                <div class="tile tile-plain tile-clickable">
                    <div class="tile-body">
                        <img src="<cms:show category_thumbnail_image />" alt="Tile here" class="thumbnail">
                    </div>
                    <div class="tile-footer">
                        <h2><cms:show category_name /></h2>
                        <cms:excerptHTML count="14" ignore="img, table, br, h1, h2, iframe"><p><cms:show category_excerpt /></p></cms:excerptHTML>
                    </div>
                    <a href="<cms:show k_page_link />" title="<cms:show k_page_title />" class="wrapped-link"></a>
                </div>
            </div>
            </cms:pages>
            <cms:pages masterpage='products-sugars.php'>
            <div class="cl">
                <div class="tile tile-plain tile-clickable">
                    <div class="tile-body">
                        <img src="<cms:show category_thumbnail_image />" alt="Tile here" class="thumbnail">
                    </div>
                    <div class="tile-footer">
                        <h2><cms:show category_name /></h2>
                        <cms:excerptHTML count="14" ignore="img, table, br, h1, h2, iframe"><p><cms:show category_excerpt /></p></cms:excerptHTML>
                    </div>
                    <a href="<cms:show k_page_link />" title="<cms:show k_page_title />" class="wrapped-link"></a>
                </div>
            </div>
            </cms:pages>
            <cms:pages masterpage='products-nuts.php'>
            <div class="cl">
                <div class="tile tile-plain tile-clickable">
                    <div class="tile-body">
                        <img src="<cms:show category_thumbnail_image />" alt="Tile here" class="thumbnail">
                    </div>
                    <div class="tile-footer">
                        <h2><cms:show category_name /></h2>
                        <cms:excerptHTML count="14" ignore="img, table, br, h1, h2, iframe"><p><cms:show category_excerpt /></p></cms:excerptHTML>
                    </div>
                    <a href="<cms:show k_page_link />" title="<cms:show k_page_title />" class="wrapped-link"></a>
                </div>
            </div>
            </cms:pages>
        </div>   
    </section>

<!-- Footer -->
<cms:embed 'footer.html' />

<?php COUCH::invoke(); ?>