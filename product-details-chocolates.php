<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title='Product Details (chocolates table)' clonable='1' dynamic_folders='1' order="6">
    
    <cms:editable
      name="assigned_product"
      label="Product Category"
      desc=""
      opt_values='Chocolates=0 | Fruits=1 | Sugars=2 | Nuts=3'
      type='dropdown' 
      order="0"
    />

    <cms:editable 
        type='relation'
        name='product_details' 
        masterpage='products-item.php' 
        has='one' 
        no_gui='0' 
        label='Related Product'
        order="1"
    />

    <!-- Chocolates -->
    <cms:editable name='product_chocolate_group' label='Chocolates' desc='detailed' type='group' order="2" />
    <cms:repeatable name='product_chocolate_table' label="Product Details" order="1" group='product_chocolate_group' >
        <cms:editable name="product_chocolate_name" label='Name' type="text" order="1" />  
        <cms:editable name="product_chocolate_code" label='Code No.' type="text" order="2" />  
        <cms:editable name="product_chocolate_hscode" label='HS Code No.' type="text" order="3" />  
        <cms:editable name="product_chocolate_origin" label='Cocoa Bean Origin' type="text" order="4" /> 
        
        <cms:editable type='dropdown' name='acidity' label='Acidity' opt_values='0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10' />
        <cms:editable type='dropdown' name='bitter' label='Bitter' opt_values='0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10' />
        <cms:editable type='dropdown' name='cacao' label='Cacao' opt_values='0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10' />
        <cms:editable type='dropdown' name='fruity' label='Fruity' opt_values='0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10' />
        <cms:editable type='dropdown' name='roasted' label='Roasted' opt_values='0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10' />
        <cms:editable type='dropdown' name='spicy' label='Spicy' opt_values='0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10' />
        <cms:editable type='dropdown' name='woody' label='Woody' opt_values='0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10' />
       
        <cms:editable name="product_chocolate_cocoa" label='Cocoa' type="text" desc="(1 - 100)" order="6" /> 
        <cms:editable name="product_chocolate_milk" label='Milk' type="text" desc="(1 - 100)" order="7" /> 
        <cms:editable name="product_chocolate_fat" label='Fat' type="text" desc="(1 - 100)" order="8" /> 
        <cms:editable name="product_chocolate_sugar" label='Sugar' type="text" desc="(1 - 100)" order="9" /> 

        <cms:editable type='dropdown' name='chocolate_fluidity' label='Fluidity' opt_values='1 | 2 | 3 | 4 | 5' order="10" />
    </cms:repeatable> 

    
    <!-- Fruits -->
    <cms:editable name='product_fruits_group' label='Fruits' desc='detailed' type='group' order="3" />
    <cms:editable type='text' name='product_fruit_percentage' label='Fruit Percentage' desc="1-100" group='product_fruits_group' />
    <cms:editable type='text' name='product_fruit_bake_percentage' label='Bake Stability Percentage' desc="1-100" group='product_fruits_group' />
    <cms:editable type='text' name='product_fruit_integrity_percentage' label='Fruit Integrity Percentage' desc="1-100" group='product_fruits_group' />
    <cms:editable type='text' name='product_fruit_sugar_percentage' label='Fruit Sugar Percentage' desc="1-100" group='product_fruits_group' />

    <cms:repeatable name='product_fruits_table' label="Product Details" order="1" group='product_fruits_group' >
        <cms:editable name="product_fruits_flavor" label='Flavor' type="text" order="1" />  
        <cms:editable name="product_fruits_origin" label='Origin(Country)' type="text" order="2" />
        <cms:editable name="product_fruits_origin_image" label='Origin(Image)' desc="150x150" type="image" quality="100" order="3" /> 
        <cms:editable name="product_fruits_form" label='Form' type="text" order="4" />  
    </cms:repeatable> 


    <!-- Sugars -->
    <cms:editable name='product_sugars_group' label='Sugars' desc='detailed' type='group' order="3" />
    <cms:repeatable name='product_sugars_table' label="Product Details" order="1" group='product_sugars_group' >
        <cms:editable name="product_sugars_name" label='Name' type="text" order="1" />  
        <cms:editable name="product_sugars_code" label='Code No.' type="text" order="2" />
        <cms:editable name="product_sugars_hscode" label='HS Code No.' type="text" order="3" />  
        <cms:editable name="product_sugars_colors_flavors" label='Color/Flavor' type="image" order="4" />  
    </cms:repeatable> 


    <!-- Nuts -->
    <cms:editable name='product_nuts_group' label='Nuts' desc='detailed' type='group' order="4" />
    <cms:repeatable name='product_nuts_table' label="Product Details" order="1" group='product_nuts_group' >
        <cms:editable name="product_nuts_name" label='Name' type="text" order="1" />  
        <cms:editable name="product_nuts_code" label='Code No.' type="text" order="2" />  
        <cms:editable name="product_nuts_hscode" label='HS Code No.' type="text" order="3" />  
        <cms:editable name="product_nuts_hazelnut" label='Fat' type="text" desc="(0 - 100)" order="8" /> 
        <cms:editable name="product_nuts_sugar" label='Sugar' type="text" desc="(0 - 100)" order="9" /> 
    </cms:repeatable> 


</cms:template>
<?php COUCH::invoke(); ?>