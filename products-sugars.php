<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="Category - GLAZES & SUGARS" order="22"> 
    <cms:editable name="category_name" label="Category name" type="text" order="0" />
    <cms:editable name="category_desc" label="Category description" type="richtext" order="1" />
    <cms:editable name="category_excerpt" label="Category excerpt" desc="Max of 100 characters (recommended)" type="textarea" order="3" />
    <cms:editable name="category_banner_image" type="image" label='Banner Image' desc="3333 x 1875" order="4" />
    <cms:editable name="category_og_image" type="image" label='Category OG Image (1200px X 630px)' order="5" />
        <cms:editable name="category_thumbnail_image" type="image" label='Category Thumbnail Image (500px X 540px)' order="6" />
    <cms:editable name="product_button_label" label='Button Label' type="text" order="7" />
    <cms:editable
      name='product_downloadable_file'
      label='Downloadable File'
      desc='Upload the file here'
      type='file'
      order="8"
    />
</cms:template>

    <!-- SubNav -->
    <cms:embed 'products-list.html' />
    <!-- /SubNav -->

    <!-- Footer -->
    <cms:embed 'footer.html' />
    <!-- /Footer -->    
<?php COUCH::invoke(); ?>    