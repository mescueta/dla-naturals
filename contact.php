<?php require_once( 'admindla/cms.php' ); ?>
<cms:template title="Contact Us" executable="1" order="24"> 
    <cms:editable name="page_title" label="Page Title" type="text" order="1"/>
    <cms:editable name="contact_label" label="Header Label" type="text" order="2" />
    <cms:editable name="contact_desc" label="Main Description" type="richtext" order="3" />
    <cms:editable name="contact_other_desc" label="Other Description" type="richtext" order="4" />
    <cms:editable name="contact_phone" label="Telephone" type="text" order="5" />
    <cms:editable name="contact_fax" label="Fax" type="text" order="6" />
    <cms:editable name="contact_email" label="Email" type="text" order="7" />
    <cms:editable name="contact_address" label="Address" type="richtext" order="8" />
    <cms:editable name="contact_gmap_embed" type="textarea" label="Google map embed" desc="(Shortcode: [googlemap src='embeded-source-here'] )" order="9" />

    <cms:repeatable name='contact_slider_images' label="Slider Images" order="10" >
       <cms:editable type='image' name='slide_image' label='Slide Image' />
    </cms:repeatable>

    
    <cms:repeatable name='contact_list' label="List of Contacts/Countries" order="11" >
        <cms:editable name="contact_list_title" label="Country" type="text" group='contact_list_group' col_width='100'/>

        <cms:editable name='NIEUWSKOLOM' label='NIEUWSKOLOM' desc='Een gestructureerde lijst van nieuws items in kolomvorm' type='group' />
        <cms:editable type='image' name='contact_list_header_image' label='Header Image' show_preview='0' quality='100' width="200" group='NIEUWSKOLOM' />
        <cms:editable type='image' name='contact_list_flag_image' label='Flag Image' show_preview='0' quality='100' group='NIEUWSKOLOM' />
        <cms:editable name="contact_list_details" label="Details" type="nicedit" />
        <cms:editable name="contact_list_map_link" label="Map linkout" type="text" col_width='100'/>
    </cms:repeatable>

</cms:template>

    <cms:set page_desc="<cms:get_custom_field 'site_desc' masterpage='globals.php' />" />
    <cms:set page_image="<cms:get_custom_field 'og_image' masterpage='globals.php' />" />
    <cms:set page_title="<cms:show page_title /> | <cms:get_custom_field 'site_name' masterpage='globals.php' />" />

    <cms:embed 'header.html' />
    
    <div class="main-container contact" id="main">

        <!-- Slider -->
        <section class="banner-section banner-secondary">
            <!-- Carousel -->
            <div class="rw carousel carousel-secondary adjusted-arrows">
                <cms:show_repeatable 'contact_slider_images' >
                    <div class="cl">
                        <div class="tile tile-magazine">
                            <div class="tile-body">
                                <img src="<cms:show slide_image />" alt="contact History" class="thumbnail">
                            </div>
                        </div>
                    </div>
                </cms:show_repeatable>
            </div>
        </section>

        <!-- Details -->
        <section class="default-section section-80">
            <div class="rw text-left section-head">
                <div class="cl cl-8">
                    <h1 class="subtitle"><cms:show contact_label /></h1>
                </div>

                <div class="cl cl-12 contact-list">
                    <div class="rw rw-full cl-4">
                        <cms:show_repeatable 'contact_list' >
                        <div class="cl">
                            <div class="tile tile-contact">
                                <div class="tile-body with-rounded-icon">
                                    <img src="<cms:show contact_list_header_image />" alt="<cms:show contact_list_title />" class="thumbnail">
                                    <div class="rounded-icon left-centered">
                                        <img src="<cms:show contact_list_flag_image />" alt="<cms:show contact_list_title />" class="wrapped-logo">
                                    </div>
                                    <div class="floated-content">
                                        <h3><cms:show contact_list_title /></h3>
                                    </div>
                                </div>
                                <div class="tile-footer">
                                    <cms:show contact_list_details />

                                    <cms:if contact_list_map_link>
                                    <a href="<cms:show contact_list_map_link />" target="_blank" class="blocked-link text-right" title="read">Go to Map <i class="icon fas fa-chevron-right"></i></a>
                                    </cms:if>
                                </div>
                            </div>
                        </div>
                        </cms:show_repeatable>
                    </div>
                </div>

                <div class="cl cl-12 contact-list" style="display: none;">
                    <div class="rw rw-full cl-4">
                        <div class="cl">
                            <div class="tile tile-contact">
                                <div class="tile-body with-rounded-icon">
                                    <img src="theme/images/dla_naturals_site-unblur.jpg" alt="Tile here" class="thumbnail">
                                    <div class="rounded-icon left-centered">
                                        <img src="theme/images/flag-ph.png" alt="logo" class="wrapped-logo">
                                    </div>
                                    <div class="floated-content">
                                        <h3>Philippines</h3>
                                    </div>
                                </div>
                                <div class="tile-footer">
                                    <p><strong>MARJORIE ANNE SISCON</strong><br>
                                    Philippines Brand Manager<br>
                                    Email: marjorie@dlanaturals.com<br>
                                    Mobile: +63917 854 3713<br>
                                    Skype: marjorie.dlanaturals</p>
                                    <br>
                                    <p><strong>BENJAMIN FANCA</strong><br>
                                    Area Export Manger<br>
                                    Email: benjamin@dlanaturals.com<br>
                                    Mobile: +6695 874 3071<br>
                                    Skype: benjamin.fanca</p>

                                    <a href="#" target="_blank" class="blocked-link text-right" title="read">Go to Map <i class="icon fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="cl">
                            <div class="tile tile-contact">
                                <div class="tile-body with-rounded-icon">
                                    <img src="theme/images/dla_naturals_site-unblur.jpg" alt="Tile here" class="thumbnail">
                                    <div class="rounded-icon left-centered">
                                        <img src="theme/images/flag-china.png" alt="logo" class="wrapped-logo">
                                    </div>
                                    <div class="floated-content">
                                        <h3>China</h3>
                                    </div>
                                </div>
                                <div class="tile-footer">
                                    <p><strong>RITA &nbsp;YANG</strong><br>
                                    China Brand Manager<br>
                                    Email: rita@dlanaturals.com<br>
                                    Mobile / Whatsapp:&nbsp;(+86) 1 37 517 69550</p>

                                    <a href="#" target="_blank" class="blocked-link text-right" title="read">Go to Map <i class="icon fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="cl">
                            <div class="tile tile-contact">
                                <div class="tile-body with-rounded-icon">
                                    <img src="theme/images/dla_naturals_site-unblur.jpg" alt="Tile here" class="thumbnail">
                                    <div class="rounded-icon left-centered">
                                        <img src="theme/images/flag-china.png" alt="logo" class="wrapped-logo">
                                    </div>
                                    <div class="floated-content">
                                        <h3>HONGKONG-MACAU</h3>
                                    </div>
                                </div>
                                <div class="tile-footer">
                                    <p><strong>GUILLAUME FRAISSINET</strong><br>
                                    Country Manager Hong Kong-Macau</p>
                                    <p>DLA Naturals Hong Kong LTD.<br>
                                    17/F, Yue Hing Building,<br>
                                    103 Hennessy Road, Wanchai, Hongkong<br>
                                    Email: guillaume@dlanaturals.com<br>
                                    Skype: guillaume.dlanaturals</p>

                                    <a href="#" target="_blank" class="blocked-link text-right" title="read">Go to Map <i class="icon fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="cl cl-12 contact-details">
                    <div class=""><cms:show contact_other_desc /></div>
                    <p><span class="highlighted">Phone:</span> <cms:show contact_phone /></p>
                    <p><span class="highlighted">Fax:</span> <cms:show contact_fax /></p>
                    <p><span class="highlighted">Email:</span> <cms:show contact_email /></p>
                </div>
                <div class="cl cl-12 contact-details">
                    <p><span class="highlighted">Corporate address:</span></p>
                    <p><cms:show contact_address /></p>
                </div>

                <div class="cl cl-12 map"> 
                    <div class="embed-wrapper">
                        <cms:do_shortcodes><cms:show contact_gmap_embed /></cms:do_shortcodes>
                    </div>
                </div>

                <div class="cl cl-12">
                    <div><cms:show contact_desc /></div>
                </div>
                <div class="cl cl-12 subscribe-wrapper">
                    <cms:form action="<cms:link 'newsletter.php'/>" method='post' name='subscribe'>
                        <div class="form-group cl bordered">
                            <!-- <i class="suffix fa fa-user"></i> -->
                            <button type="submit" class="suffix btn btn-default">Join</button>
                            <cms:input name='email' placeholder='Email Address' required='1' type='text' validator='email' class="validate" id="borderedicon_prefix"/>
                            <!-- <label for="borderedicon_prefix">Join</label> -->
                        </div>  
                    </cms:form>
                </div>


                <!-- Old details -->
                <div style="display: none;">
                    <p><u><strong>PHILIPPINES</strong></u></p>

<p><strong>MARJORIE ANNE SISCON</strong><br />
Philippines Brand Manager<br />
Email: marjorie@dlanaturals.com<br />
Mobile: +63917 854 3713<br />
Skype: marjorie.dlanaturals</p>

<p><br />
<strong>BENJAMIN FANCA</strong><br />
Area Export Manger<br />
Email: benjamin@dlanaturals.com<br />
Mobile: +6695 874 3071<br />
Skype: benjamin.fanca</p>

<p>&nbsp;</p>

<p><u><strong>CHINA</strong></u></p>

<p><strong>RITA &nbsp;YANG</strong><br />
China Brand Manager<br />
Email: rita@dlanaturals.com<br />
Mobile / Whatsapp:&nbsp;(+86) 1 37 517 69550</p>

<p>&nbsp;</p>

<p><strong><u>HONGKONG-MACAU</u></strong></p>

<p><strong>GUILLAUME FRAISSINET</strong><br />
Country Manager Hong Kong-Macau</p>

<p>DLA Naturals Hong Kong LTD.<br />
17/F, Yue Hing Building,<br />
103 Hennessy Road, Wanchai, Hongkong<br />
Email: guillaume@dlanaturals.com<br />
Skype: guillaume.dlanaturals</p>

                </div>

            </div>

        </section>


        <!-- Factory Map -->
        <section class="default-section section-80"> 
            <div class="rw cl-1">
                <div class="cl">
                    <cms:show contact_embed />
                </div>
            </div>        
        </section> 
            
    <cms:embed 'footer.html' />
    <!-- /Footer -->    

<?php COUCH::invoke(); ?>    