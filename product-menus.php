<?php require_once( 'admindla/cms.php' ); ?> 
<cms:template title="Menu - PRODUCTS" clonable="1" order="25" hidden="0"> 
    
    <cms:editable name='menu_link' label='Link' type='text' order='0' />

    <cms:config_form_view>
        <cms:field 'k_page_title' desc='Label' label='Label' order='0' />
        <cms:field 'k_page_name' skip='1'/>
    </cms:config_form_view>

    <cms:config_list_view orderby='weight' order='desc'>
        <cms:field 'k_selector_checkbox' />
        <cms:field 'k_page_title' sortable='0' />
        <cms:field 'k_up_down' header='Sort Manually' />
        <cms:field 'k_actions' />
    </cms:config_list_view>

</cms:template>

<?php COUCH::invoke(); ?>    
